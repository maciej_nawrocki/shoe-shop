package main.java;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "welcome")
public class WelcomeBean {
    public WelcomeBean() {
        System.out.println("WelcomeBean instantiated");
    }
    public String getMessage() {
        return "I'm alive!";
    }
}